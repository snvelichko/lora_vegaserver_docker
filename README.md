# Install

```shell
helm upgrade --install --wait --set vegaserver.ingress.url=http://vegaserver.domain.name --set vegaadmintool.ingress.url=http://vegaadmintool.domain.name --namespace=iot-vega iot-vega iot-vega-server
```